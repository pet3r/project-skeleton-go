package service

import (
	"strconv"
	"testing"
)

// TestDummyWithTables is an example table driven unit test
func TestDummyWithTables(t *testing.T) {
	for index, data := range []struct {
		x        int
		y        int
		expected int
	}{
		{1, 1, 2},
		{1, 2, 3},
		{2, 2, 4},
		{5, 2, 7},
	} {
		d := data
		t.Run("Test Case "+strconv.Itoa(index), func(t *testing.T) {
			total := sum(d.x, d.y)
			if total != d.expected {
				t.Fatalf("Sum of (%d+%d) was incorrect, got: %d, want: %d.", d.x, d.y, total, d.expected)
			}
		})

	}
}

func sum(x int, y int) int {
	return x + y
}
