package rest

import (
	"github.com/pkg/errors"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/schema"
	"gitlab.com/proemergotech-public/project-skeleton-go/errorsf"
)

type routeNotFoundError struct {
	Err error
}

func (e routeNotFoundError) E() error {
	return errorsf.WithFields(
		errors.Wrap(e.Err, "route cannot be found"),
		schema.ErrHTTPCode, 404,
		schema.ErrCode, schema.ErrRouteNotFound,
	)
}

type methodNotAllowedError struct {
	Err error
}

func (e methodNotAllowedError) E() error {
	return errorsf.WithFields(
		errors.Wrap(e.Err, "method not allowed"),
		schema.ErrHTTPCode, 405,
		schema.ErrCode, schema.ErrMethodNotAllowed,
	)
}
