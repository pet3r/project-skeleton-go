package config

// AppName of the application
const AppName = "project-skeleton-go"

// AppVersion Version of the application
var AppVersion string

type Config struct {
	LogLevel string `mapstructure:"log_level" default:"info"`

	Port int `mapstructure:"port" default:"80"`

	RedisHost            string `mapstructure:"redis_host"`
	RedisPort            int    `mapstructure:"redis_port" default:"6379"`
	RedisDatabase        int    `mapstructure:"redis_database"`
	RedisPoolMaxIdle     int    `mapstructure:"redis_pool_max_idle" default:"10"`
	RedisPoolIdleTimeout string `mapstructure:"redis_pool_idle_timeout" default:"240s"`
}
