package di

import (
	"context"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/go-playground/validator"
	"github.com/gomodule/redigo/redis"
	jsoniter "github.com/json-iterator/go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/pkg/errors"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/config"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/rest"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/service"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/storage"
	"gitlab.com/proemergotech-public/project-skeleton-go/app/validationerr"
	"gitlab.com/proemergotech-public/project-skeleton-go/log"
)

type Container struct {
	RestServer *rest.Server
	redis      *storage.Redis
}

type EchoValidator struct {
	validator *validator.Validate
}

func (cv *EchoValidator) Validate(i interface{}) error {
	err := cv.validator.Struct(i)
	if err != nil {
		return validationerr.ValidationError{Err: err}.E()
	}

	return nil
}

func NewContainer(cfg *config.Config) (*Container, error) {
	c := &Container{}

	var err error
	c.redis, err = newRedis(cfg)
	if err != nil {
		return nil, errors.Wrap(err, "cannot initialize redis client")
	}

	validate := newValidator()

	echoEngine := newEcho(validate)

	svc := service.NewService(c.redis)

	c.RestServer = rest.NewServer(
		cfg.Port,
		echoEngine,
		rest.NewController(
			echoEngine,
			svc,
		),
	)

	return c, nil
}

func newRedis(cfg *config.Config) (*storage.Redis, error) {
	redisPool, err := newRedisPool(cfg)
	if err != nil {
		return nil, err
	}

	redisJSON := jsoniter.Config{
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		OnlyTaggedField:        true,
		TagKey:                 "redis",
	}.Froze()

	return storage.NewRedis(redisPool, redisJSON), nil
}

func newRedisPool(cfg *config.Config) (*redis.Pool, error) {
	redisPoolIdleTimeout, err := time.ParseDuration(cfg.RedisPoolIdleTimeout)
	if err != nil {
		return nil, errors.New("invalid value for redis_pool_idle_timeout, must be duration")
	}

	return &redis.Pool{
		MaxIdle:     cfg.RedisPoolMaxIdle,
		IdleTimeout: redisPoolIdleTimeout,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", fmt.Sprintf("%v:%v", cfg.RedisHost, cfg.RedisPort), redis.DialDatabase(cfg.RedisDatabase))
		},
	}, nil
}

func newValidator() *validator.Validate {
	v := validator.New()

	v.RegisterTagNameFunc(func(field reflect.StructField) string {
		name := strings.SplitN(field.Tag.Get("json"), ",", 2)[0]

		if name == "-" {
			name = ""
		}

		return name
	})

	return v
}

func newEcho(validate *validator.Validate) *echo.Echo {
	e := echo.New()

	e.Use(middleware.Recover())
	e.HTTPErrorHandler = rest.DLiveRHTTPErrorHandler
	e.Validator = &EchoValidator{validator: validate}

	return e
}

func (c *Container) Close() {
	err := c.redis.Close()
	if err != nil {
		err = errors.Wrap(err, "redis graceful close failed")
		log.Warn(context.Background(), err.Error(), "error", err)
	}
}
