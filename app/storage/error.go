package storage

import (
	"github.com/pkg/errors"

	"gitlab.com/proemergotech-public/project-skeleton-go/app/schema"
	"gitlab.com/proemergotech-public/project-skeleton-go/errorsf"
)

type redisError struct {
	Err error
	Msg string
}

func (e redisError) E() error {
	msg := "redis error"
	if e.Msg != "" {
		msg += ": " + e.Msg
	}

	err := e.Err
	if err == nil {
		err = errors.New(msg)
	} else {
		err = errors.Wrap(err, msg)
	}

	return errorsf.WithFields(
		err,
		schema.ErrCode, schema.ErrRedis,
		schema.ErrHTTPCode, 500,
	)
}
