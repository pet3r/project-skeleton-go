ARG EXECUTABLE_NAME=project-skeleton-go

FROM golang:1.12.5-alpine AS builder

ARG APP_VERSION
ARG EXECUTABLE_NAME

ENV ROOT_PACKAGE=gitlab.com/proemergotech-public/$EXECUTABLE_NAME
ENV DEP_VERSION=0.5.0

RUN apk add --update --no-cache wget openssh-client git
RUN wget -O /usr/local/bin/dep https://github.com/golang/dep/releases/download/v$DEP_VERSION/dep-linux-amd64 && chmod +x /usr/local/bin/dep

ADD . $GOPATH/src/$ROOT_PACKAGE
WORKDIR $GOPATH/src/$ROOT_PACKAGE

RUN dep ensure -vendor-only

RUN go build -ldflags "-X $ROOT_PACKAGE/app/config.AppVersion=$APP_VERSION" -o "/tmp/$EXECUTABLE_NAME"




FROM alpine:latest

ARG EXECUTABLE_NAME

RUN set -eux; \
  apk add --no-cache ca-certificates curl

WORKDIR /usr/local/bin/

COPY --from=builder /tmp/$EXECUTABLE_NAME ./$EXECUTABLE_NAME
RUN chmod +x ./$EXECUTABLE_NAME

EXPOSE 80

# can't use variables here, there is no shell to interpret them
CMD ["project-skeleton-go"]
