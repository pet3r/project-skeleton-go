# Test assignment

## Overview

Create a project, that periodically monitors a gitlab project's merge requests, using the gitlab api. If it finds a new
merge request, it must set squash commits to true for it, and store the processed merge request's id in redis.

## Requirements
- copy and use https://gitlab.com/proemergotech-public/project-skeleton-go as a base
- accept a `GITLAB_PROJECT_NAME` via environment variable
- accept any needed credentials via environment variables
- accept `POLL_PERIOD` via environment variable
- every `POLL_PERIOD` interval call gitlab api to fetch new merge request
- if there is a new merge request
	- set squash commits to true for it
	- and store the merge request id in redis
- add a http `GET` route `/api/v1/merge-requests` which returns a json array containing the processed merge request ids
- make your project publicly accessible on github or gitlab
- the project MUST be startable with docker-compose (the project skeleton already contains a docker-compose file for this)
  - docker-compose must be configured to use `GITLAB_PROJECT_NAME=proemergotech-public/project-skeleton-go`

# Project skeleton

## Overview

Project skeleton based on our latest standards.
Provides a basic golang based rest service.

## Usage

Copy the skeleton to the desired directory, then run a search-and-replace in the project:
- replace all occurrences of `gitlab.com/proemergotech-public` to `<github.com|gitlab.com>/<your organization>`
- replace all occurrences of `project-skeleton-go` to `<your project name>`

# Using the created project

## Project dependencies

We use [dep](https://github.com/golang/dep) for dependency management. Simply install it and run `dep ensure` in the project root.
It may take considerable time (10 minutes, or even more) the first time you run it.

## Build modes

There are 2 ways to build the project, you can see both of them in the docker-compose.yml (production build is commented out):
1. development mode, using the dev/Dockerfile
    - In this mode the source files must be attached using a docker volume. The binary is automatically rebuilt
    every time the source files are modified. See example in docker-compose.yml.
2. production mode
    - In this mode the binary is built and copied to a bare alpine image. The image can be run as-is. See example
    commented out in docker-compose.yml.

## Running the project

After installing dependencies, run:
- `docker-compose up -d`
 
## Verifying the build

You can verify that everything works as expected:
- `curl localhost:43212/healthcheck` should return `ok`
- `curl localhost:43212/api/v1/dummy` should return `null`
